MTA Hosting Optimizer
=====================

Short Description
-----------------

As an operations engineer I want to create MTA Hosting Optimization service that uncovers the inefficient servers hosting only few active MTAs.

Requirements
------------

* Python3
* SQLite

Quick Start
-----------

1. `pip install virtualenv`
2. `virtualenv name_of_virtual_environment`
3. Now go to your terminal. Go to the directory that contains the file called `activate`. The file can be found inside folder called `bin` for macOS and Linux.
4. `name_of_virtual_environmnet/bin/activate`
5. `pip install -r requirments.txt`
6. `python app.py`
7. Open http://127.0.0.1:5000/host/ in browser. You can set your dynamic parameter like http://127.0.0.1:5000/host/?threshold=3

Dockerization
-------------

* Docker file provided in the repository `Dockerfile`.

GitLab Build Pipeline
---------------------

* Build pipeline file has been given in the repository `.gitlab-ci.yml`.

Rancher Deployment
------------------

1. HAproxy Loadbalancer exists.
2. Two instances of the application are running at the same time.
3. Only one service container of the application must run per host.
4. `docker-compose.yml​` file describing general service deployment. `rancher-compose.yml​` file describing rancher specific service deployment. Both are exists in the repository.
